import {PRODENV} from './prod.env';
import { DEVENV } from './dev.env';

const envs = {
    production: PRODENV,
    development: DEVENV
}

export const env = envs[process.env.NODE_ENV];