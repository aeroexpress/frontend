export const REQUIRED = (value: any) => String(value).length > 0 && value !== null || 'Это обязательное поле';

export const EMAIL = (value: string) => {
    const pattern = /^\S+@\S+\.\w{2,}$/;
    return pattern.test(value) || value.length === 0 || value === undefined || 'Не валидный емайл';
};

export const PASSWORD = (value: string) => /^\w{5,50}$/.test(value) && /\d+/.test(value) || value === undefined || 'Пароль должен содержать латинские буквы и хоть одну цифру. Всего символов от 5 до 50';

export const LOGIN = (value: string) => /^\w{4,50}$/.test(value) || value === undefined || 'Invalid';
