export class TokenRepository {

    public static setToken(token: string): void {
        localStorage.setItem('token', token);
    }

    public static getToken(): any {
        return localStorage.getItem('token');
    }

    public static removeToken(): void {
        localStorage.removeItem('token');
    }

}