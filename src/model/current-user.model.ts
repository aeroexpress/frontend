export class CurrentUserModel {
    last_name: string = '';
    first_name: string = '';
    middle_name: string = '';
    email: string = '';
    role: string = '';
}