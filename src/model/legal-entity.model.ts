export class LegalEntityModel {
    profile = {
        id: null,
        first_name: '',
        last_name: '',
        middle_name: '',
        phone: '',
        email: '',
        password: '',
        role: 'role_user',
    }
    account = {
        id: null,
        number: '',
        correspondent_account: '',
        bik: '',
        bank: '',
    }
    contract = {
        id: null,
        start_date: '',
        end_date: '',
        number: '',
        is_credit_limit_enabled: false,
        credit_amount: '',
    }
    legal_entity = {
        id: null,
        full_name: '',
        legal_address: '',
        actual_address: '',
        inn: '',
        kpp: '',
        ogrn: ''
    }
}