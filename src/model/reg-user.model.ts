export class RegUserModel {
    first_name: string = '';
    last_name: string = '';
    middle_name: string = '';
    phone: string = '';
    email: string = '';
    password: string = '';
}