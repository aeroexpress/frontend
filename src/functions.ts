export function getPaymentType(type: string): string {
    switch(type) {

        case 'through_accounts':
            return 'Счет';

        case 'online_pay':
            return 'Онлайн';
        
        case 'credit':
            return 'Кредит';
        
        case 'business_card':
                return 'Бизнес карта';

        default:
            return type;
        
    }
}

export function getRate(rate: string): string {
    switch(rate) {

        case 'standart':
            return 'Туда - Стандарт';

        case 'business':
            return 'Туда - Бизнес';
        
        case 'round_trip_standart':
            return 'Туда - Обратно - Стандарт';
        
        case 'round_trip_business':
                return 'Туда - Обратно - Бизнес';

        default:
            return rate;
        
    }
}