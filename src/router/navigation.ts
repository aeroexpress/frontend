import { NavigationModel } from '@/model/navigation.model';
import OrdersPage from '../page/orders/orders.page.vue';
import LoginPage from '../page/login/login.page.vue';
import RegistrationPage from '../page/login/registration.page.vue';
import UsersPage from '../page/users/users.page.vue';
import EditLegalEntityPage from '../page/users/edit-legal-entity.page.vue';
import RoutePage from '../page/route.page.vue';
import NewOrderPage from '../page/orders/new-order.page.vue';
import ProfilePage from '../page/profile.page.vue';
import ErrorPage from '../page/error.page.vue';
import OrderInfoPage from '../page/orders/order-info.page.vue';
import ManagerDashboardPage from '../page/dashboard/manager-dashboard.page.vue';

export const navigation: NavigationModel[] = [

    {
        path: '/manager-dashboard',
        name: 'Статистика',
        component: ManagerDashboardPage,
        meta: {
            icon: 'mdi-finance',
            showInMenu: true,
            roles: ['role_manager']
        }
    },

    {
        path: '/orders',
        name: 'Заказы',
        component: RoutePage,
        meta: {
            icon: 'mdi-cart-outline',
            showInMenu: true,
            roles: ['role_user']
        },
        children: [
            {
                path: '',
                name: '',
                component: OrdersPage,
                meta: {
                    icon: 'mdi-cart-outline',
                    roles: ['role_user']
                }
            },
            {
                path: 'new',
                name: '',
                component: NewOrderPage,
                meta: {
                    icon: 'mdi-cart-outline',
                    roles: ['role_user']
                }
            },
            {
                path: ':id',
                name: '',
                component: OrderInfoPage,
                meta: {
                    icon: 'mdi-cart-outline',
                    roles: ['role_user']
                }
            },
        ]
    },
    
    {
        path: '/users',
        name: 'Пользователи',
        component: RoutePage,
        meta: {
            icon: 'mdi-account-multiple',
            showInMenu: true,
            roles: ['role_manager']
        },
        children: [
            {
                path: '',
                name: '',
                component: UsersPage,
                meta: {
                    roles: ['role_manager']
                }
            },
            {
                path: ':id',
                name: 'Новый пользователь',
                component: EditLegalEntityPage,
                meta: {
                    roles: ['role_manager']
                }
            }
        ]
    },

    {
        path: '/profile',
        name: 'Профиль',
        component: ProfilePage,
        meta: {
            showInMenu: true,
            icon: 'mdi-account',
            roles: ['role_user']
        }
    },

    // {
    //     path: '/profile',
    //     name: 'Профиль',
    //     component: ProfilePage,
    //     meta: {
    //         showInMenu: true,
    //         icon: 'mdi-account',
    //         roles: ['role_manager']
    //     }
    // },

    {
        path: '/error/:status',
        name: 'Ошибка',
        component: ErrorPage,
        meta: {
            roles: []
        }
    },

    {
        path: '/login',
        name: 'Логин',
        component: LoginPage,
        meta: {
            roles: []
        }
    },

    {
        path: '/registration',
        name: 'регистрация',
        component: RegistrationPage,
        meta: {
            roles: []
        }
    }

]