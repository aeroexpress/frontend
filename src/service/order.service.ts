import { BaseService } from './base.service';
import { AxiosResponse } from 'axios';

export class OrderService extends BaseService {

    downloadDefaultExcel(): Promise<AxiosResponse<any>> {
        return this.loginedHttp.get('/get_xlsx');
    }

    uploadExcel(rate: string, file: File): Promise<AxiosResponse<any>> {
        let formData:FormData = new FormData();
        formData.append('file', file);
        formData.append('rate', rate);
        return this.loginedHttp.post('/update_xlsx/', formData);
    }

    getCreditLimit(): Promise<AxiosResponse<{limit: number}>> {
        return this.loginedHttp.get('/get_credit_limit');
    }

    createOrder(order: any): Promise<AxiosResponse<{credit_lemit: number, total_amount: number}>> {
        return this.loginedHttp.post('/create_order/', order);
    }

    downloadInvoice(): Promise<AxiosResponse<any>> {
        return this.loginedHttp.get('/download_invoice');
    }

    downloadAct(): Promise<AxiosResponse<any>> {
        return this.loginedHttp.get('/download_act');
    }
    
    downloadTicket(): Promise<AxiosResponse<any>> {
        return this.loginedHttp.get('/download_ticket');
    }

    downloadQR(): Promise<AxiosResponse<any>> {
        return this.loginedHttp.get('/download_qr');
    }

    getOrderList(): Promise<AxiosResponse<any>> {
        return this.loginedHttp.get('/orders');
    }

    getOrderInfo(id: number): Promise<AxiosResponse<any>> {
        return this.loginedHttp.get('/order_data/' + id);
    }

}