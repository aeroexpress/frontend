import { BaseService } from './base.service';
import { RegUserModel } from '@/model/reg-user.model';
import { CurrentUserModel } from '@/model/current-user.model';
import { AxiosResponse } from 'axios';
import { LegalEntityModel } from '@/model/legal-entity.model';

export class LoginService extends BaseService {

    public registration(user: RegUserModel): Promise<AxiosResponse<any>> {
        return this.http.post('/profiles/', user);
    }

    public getUsers(): Promise<any> {
        return this.http.get('/profiles/');
    }

    public login(email: string, password: string): Promise<AxiosResponse<{uuid: string}>> {
        return this.http.post<{uuid: string}>('/login/', {
            email: email,
            password: password
        })
    }

    public getCurrentUser(): Promise<AxiosResponse<CurrentUserModel>> {
        return this.loginedHttp.get('/user_data/');
    }

    public createLegalEntity(entity: LegalEntityModel): Promise<AxiosResponse<LegalEntityModel>> {
        return this.loginedHttp.post('/create_legal_entity/', entity);
    }

    public getLegalEntity(id: number): Promise<AxiosResponse<LegalEntityModel>> {
        return this.loginedHttp.get(`/create_legal_entity/${id}`);
    }

    public updateLegalEntity(id: number, entity: LegalEntityModel): Promise<AxiosResponse<LegalEntityModel>> {
        return this.loginedHttp.put(`/create_legal_entity/${id}/`, entity)
    }

    public getAllLegalEntity(): Promise<AxiosResponse<any>> {
        return this.loginedHttp.get(`/legal_entities/`);
    }

    public getCurrentLegalEntity(): Promise<AxiosResponse<any>> {
        return this.loginedHttp.get(`/create_legal_entity/0`);
    }

}