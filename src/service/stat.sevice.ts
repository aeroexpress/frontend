import { BaseService } from './base.service';
import { AxiosResponse } from 'axios';

export class StatService extends BaseService {

    getStat(): Promise<AxiosResponse<any>> {
        return this.loginedHttp.get('/statistic');
    }

}