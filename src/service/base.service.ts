import axios, {AxiosInstance, AxiosRequestConfig, AxiosPromise} from 'axios';
import {env} from '../env/env';
import {TokenRepository} from '../repository/token.repository';

export class BaseService {

    get http(): AxiosInstance {
        return axios.create({
            baseURL: env.baseURL,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    get loginedHttp(): AxiosInstance {
        return axios.create({
            baseURL: env.baseURL,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': TokenRepository.getToken(),
            }
        });
    }
    
}